<?php

namespace App\Commands;

use App\DesignPattern\SimpleProxy;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;
use function Termwind\{render};

class InspireCommand extends Command
{
    protected $signature = 'inspire {name=Artisan}';

    protected $description = 'Display an inspiring quote';

    public function handle(): int
    {
        render(<<<'HTML'
            <div class="py-1 ml-2">
                <div class="px-1 bg-blue-300 text-black">Laravel Zero</div>
                <em class="ml-1">
                  Simplicity is the ultimate sophistication.
                </em>
            </div>
        HTML);

        $user = new SimpleProxy(new User(
            username: 'linkdisk',
            password: 'wonderful',
            fullName: 'Muhajirin Ilyas',
            birthday: Carbon::createFromDate(1999, 05, 23),
        ));

        render(
            <<<HTML
            <div>
                <h1>Here is the user information via proxy</h1>
                <ul>
                    <li>Username: $user->username</li>
                    <li>Password: $user->password</li>
                </ul>
                <br>
                <em class="bg-amber-300 text-black">We can see password attribute is modified through simple proxy here.</em>
            </div>
            HTML
        );

        return 0;
    }
}
