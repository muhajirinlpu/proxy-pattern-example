<?php

namespace App\DesignPattern;

class SimpleProxy
{
    public function __construct(
        protected object $payload
    ) {}

    public function __get(string $name): mixed
    {
        if ($this->__isset($name)) {
            if (in_array($name, ['password', 'secret', 'fire'], true)) {
                return password_hash($this->payload->{$name}, PASSWORD_ARGON2ID);
            }

            return $this->payload->{$name};
        }

        return null;
    }

    public function __set(string $name, $value): void
    {
        $this->payload->{$name} = $value;
    }

    public function __isset(string $name): bool
    {
        return property_exists($this->payload, $name);
    }
}
