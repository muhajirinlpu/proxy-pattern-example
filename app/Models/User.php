<?php

namespace App\Models;

use Carbon\Carbon;

class User
{
    public function __construct(
        public string $username,
        public string $password,
        public string $fullName,
        public Carbon $birthday,
    ) {}
}
